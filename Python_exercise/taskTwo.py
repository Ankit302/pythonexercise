import json
from collections.abc import Iterable
def remove_json_element(ele):
  with open('test_payload.json', 'r') as f:
    data = json.load(f)
  if ele in data:
    del data[ele]
  for i in data:
    if isinstance(data[i], Iterable):
      if ele in data[i]:
        del data[i][ele]
  json_object = json.dumps(data, indent = 4)
  with open("test_payload_modified.json", "w") as outfile:
    outfile.write(json_object)
remove_json_element('appdate')