from datetime import datetime
from datetime import timedelta
from datetime import date
import xml.etree.ElementTree as ET
def set_depart_and_return(x, y):
    today = date.today()
    mytree = ET.parse('test_payload1.xml')
    root = mytree.getroot()
    for depart in root.iter('DEPART'):
        depart.text = str(int((today + timedelta(days=x)).strftime("%Y%m%d")))
    for rtn in root.iter('RETURN'):
        rtn.text = str(int((today + timedelta(days=y)).strftime("%Y%m%d")))        
    mytree.write('test_payload1_modified.xml')    
set_depart_and_return(4, 5)
