import pandas as pd
from datetime import timezone
import pytz
pd.options.mode.chained_assignment = None
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
with open(r"Jmeter_log1.jtl", encoding="ISO-8859-1") as f:
    df_new = pd.read_csv(f)
    data = df_new.loc[df_new["responseCode"] != 200]
    df1 = data[['timeStamp', 'label', 'responseCode', 'responseMessage', 'failureMessage']]
    df1['timeStamp'] = pd.to_datetime(df1['timeStamp'])
    df1['timeStamp'] = df1['timeStamp'].dt.tz_localize(timezone.utc)
    my_timezone = pytz.timezone('US/Pacific')
    df1['timeStamp'] = df1['timeStamp'].dt.tz_convert(my_timezone)
    df1['timeStamp'] = df1['timeStamp'].dt.strftime('%Y-%m-%d %H:%m:%S %Z')
    print(df1)